<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Routing\Route as RoutingRoute;

use App\Http\Controllers\GameController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RankingController;

Route::middleware(['cors'])->group(function() {
    Route::apiResource('/games', GameController::class);
    Route::apiResource('/questions', QuestionController::class);
    Route::apiResource('/users', UserController::class);
    Route::apiResource('/ranking', RankingController::class);

    Route::get('/games/{id}/restore', GameController::class . '@restore');
    Route::get('/questions/{id}/restore', QuestionController::class . '@restore');
    Route::get('/users/{id}/restore', UserController::class . '@restore');
    Route::get('/ranking/{id}/restore', RankingController::class . '@restore');
});

