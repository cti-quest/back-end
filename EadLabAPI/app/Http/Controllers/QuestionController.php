<?php

namespace App\Http\Controllers;

use App\API\Model\Question;
use Illuminate\Http\Request;
use App\Http\Requests\QuestionCreateRequest;
use App\Http\Requests\QuestionUpdateRequest;
use App\API\Resources\Question as QuestionResource;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    public function index()
    {
        return QuestionResource::collection(Question::get());
    }

    public function store(QuestionCreateRequest $request)
    {
        $question = DB::transaction( function () use ($request){
            $question = Question::create($request->validated());
            return $question;
        });
        return QuestionResource::make($question);
    }

    public function show(Question $question)
    {
        return QuestionResource::make($question);
    }

    public function update(QuestionUpdateRequest $request, Question $question)
    {
        $editedQuestion = DB::transaction( function () use ($request, $question){
            $question->update($request->validated());
            return $question;
        });
        return QuestionResource::make($editedQuestion);
    }

    public function destroy(Question $question)
    {
        $question->delete();
    }

    public function restore($id)
    {
        Question::onlyTrashed()->where('id', $id)->restore();
    }
}
