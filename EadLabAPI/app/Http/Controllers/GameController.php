<?php

namespace App\Http\Controllers;

use App\API\Model\Game;
use Illuminate\Http\Request;
use App\Http\Requests\GameCreateRequest;
use App\Http\Requests\GameUpdateRequest;
use App\API\Resources\Game as GameResource;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{

    public function index()
    {
        return GameResource::collection(Game::get());
    }

    public function store(GameCreateRequest $request)
    {
        $game = DB::transaction( function () use ($request){
            $game = Game::create($request->validated());
            return $game;
        });
        return GameResource::make($game);
    }

    public function show(Game $game)
    {
        return GameResource::make($game);
    }

    public function update(GameUpdateRequest $request, Game $game)
    {
        $editedGame = DB::transaction( function () use ($request, $game){
            $game->update($request->validated());
            return $game;
        });
        return GameResource::make($editedGame);
    }

    public function destroy(Game $game)
    {
        $game->delete();
    }

    public function restore($id)
    {
        Game::onlyTrashed()->where('id', $id)->restore();
    }
}
