<?php

namespace App\Http\Controllers;

use App\API\Model\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\API\Resources\User as UserResource;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        return UserResource::collection(User::get());
    }

    public function store(UserCreateRequest $request)
    {
        $user = DB::transaction( function () use ($request){
            $user = User::create($request->validated());
            return $user;
        });
        return UserResource::make($user);
    }

    public function show(User $user)
    {
        return UserResource::make($user);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $editedUser = DB::transaction( function () use ($request, $user){
            $user->update($request->validated());
            return $user;
        });
        return UserResource::make($editedUser);
    }

    public function destroy(User $user)
    {
        $user->delete();
    }

    public function restore($id)
    {
        User::onlyTrashed()->where('id', $id)->restore();
    }
}
