<?php

namespace App\Http\Controllers;

use App\API\Model\Ranking;
use Illuminate\Http\Request;
use App\Http\Requests\RankingCreateRequest;
use App\Http\Requests\RankingUpdateRequest;
use App\API\Resources\Ranking as RankingResource;
use Illuminate\Support\Facades\DB;

class RankingController extends Controller
{
    public function index()
    {
        return RankingResource::collection(Ranking::get());
    }

    public function store(RankingCreateRequest $request)
    {
        $ranking = DB::transaction( function () use ($request){
            $ranking = Ranking::create($request->validated());
            return $ranking;
        });
        return RankingResource::make($ranking);
    }

    public function show(Ranking $ranking)
    {
        return RankingResource::make($ranking);
    }

    public function update(RankingUpdateRequest $request, Ranking $ranking)
    {
        $editedRanking = DB::transaction( function () use ($request, $ranking){
            $ranking->update($request->validated());
            return $ranking;
        });
        return RankingResource::make($editedRanking);
    }

    public function destroy(Ranking $ranking)
    {
        $ranking->delete();
    }

    public function restore($id)
    {
        Ranking::onlyTrashed()->where('id', $id)->restore();
    }
}
