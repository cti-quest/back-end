<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameCreateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'type'  => ['required', 'min:3', 'max:40'],
            'title' => ['required', 'min:3', 'max:40'],
            'goals' => ['required', 'min:3'],
        ];
    }
}
