<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameUpdateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'type'  => ['sometimes', 'required', 'min:3', 'max:40'],
            'title' => ['sometimes', 'required', 'min:3', 'max:40'],
            'goals' => ['sometimes', 'required', 'min:3'],
        ];
    }
}
