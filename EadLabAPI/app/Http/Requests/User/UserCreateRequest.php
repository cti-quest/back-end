<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'name'          => ['required', 'min:3', 'max:40'],
            'password'      => ['required', 'min:6'],
            'email'         => ['required', 'min:10', 'max:40', 'unique:users'],
            'total_score'   => ['required', 'integer'],
            'role'          => ['sometimes', 'required'],
        ];
    }


}
