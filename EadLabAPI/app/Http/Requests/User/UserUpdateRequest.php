<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'name'          => ['sometimes', 'required', 'min:3', 'max:40'],
            'password'      => ['sometimes', 'required', 'min:6'],
            'email'         => ['sometimes', 'required', 'min:10', 'max:40', 'unique:users'],
            'total_score'   => ['sometimes', 'required', 'integer'],
            'role'          => ['sometimes', 'required'],
        ];
    }
}
