<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'game_id'       => ['sometimes', 'required', 'integer'],
            'question'      => ['sometimes', 'required', 'min:3', 'max:140'],
            'alternative_0' => ['sometimes', 'required', 'min:1', 'max:140'],
            'alternative_1' => ['sometimes', 'required', 'min:1', 'max:140'],
            'alternative_2' => ['sometimes', 'required', 'min:1', 'max:140'],
            'alternative_3' => ['sometimes', 'required', 'min:1', 'max:140'],
            'answer'        => ['sometimes', 'required', 'min:1', 'max:1'],
        ];
    }
}
