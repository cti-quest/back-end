<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'game_id'       => ['required', 'integer'],
            'question'      => ['required', 'min:3', 'max:140'],
            'alternative_0' => ['required', 'min:1', 'max:140'],
            'alternative_1' => ['required', 'min:1', 'max:140'],
            'alternative_2' => ['required', 'min:1', 'max:140'],
            'alternative_3' => ['required', 'min:1', 'max:140'],
            'answer'        => ['required', 'min:1', 'max:140'],
        ];
    }
}
