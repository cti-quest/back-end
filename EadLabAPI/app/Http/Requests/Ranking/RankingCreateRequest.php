<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RankingCreateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'game_id'   => ['required', 'integer'],
            'user_id'   => ['required', 'integer'],
            'score'     => ['required', 'integer'],
            'time'      => ['required', 'date'],
        ];
    }
}
