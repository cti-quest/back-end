<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RankingUpdateRequest extends FormRequest
{
    public function rules() : array
    {
        return [
            'game_id'   => ['sometimes', 'required', 'integer'],
            'user_id'   => ['sometimes', 'required', 'integer'],
            'score'     => ['sometimes', 'required', 'integer'],
            'time'      => ['sometimes', 'required', 'date'],
        ];
    }
}
