<?php

namespace App\API\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Question extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'game_id' => $this->game_id,
            'question' => $this->question,
            'alternative_0' => $this->alternative_0,
            'alternative_1' => $this->alternative_1,
            'alternative_2' => $this->alternative_2,
            'alternative_3' => $this->alternative_3,
            'answer' => $this->answer,
        ];
    }
}
