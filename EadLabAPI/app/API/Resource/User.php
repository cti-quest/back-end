<?php

namespace App\API\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use App\API\Resources\Ranking as RankingResource;

class User extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'total_score' => $this->total_score,
            'role' => $this->role,
            'ranking'  => RankingResource::collection($this->whenLoaded('ranking')),
        ];
    }

}
