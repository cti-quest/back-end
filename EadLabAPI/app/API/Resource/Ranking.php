<?php

namespace App\API\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\API\Resources\Game as GameResource;
use App\API\Resources\User as UserResource;

class Ranking extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'game_id' => $this->game_id,
            'user_id' => $this->user_id,
            'score' => $this->score,
            'time' => $this->time,
            'game' => GameResource::collection($this->whenLoaded('game')),
            'user' => UserResource::collection($this->whenLoaded('user')),
        ];
    }
}
