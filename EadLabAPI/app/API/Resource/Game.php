<?php

namespace App\API\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\API\Resources\Question as QuestionResource;

class Game extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'title' => $this->title,
            'goals' => $this->goals,
            'question' => QuestionResource::collection($this->whenLoaded('question')),
        ];
    }
}
