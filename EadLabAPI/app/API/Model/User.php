<?php


namespace App\API\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable=[
        'id',
        'name',
        'email',
        'password',
        'total_score',
        'role',
    ];

    public function ranking() : BelongsTo
    {
        return $this->belongsTo(Ranking::class);
    }
}
