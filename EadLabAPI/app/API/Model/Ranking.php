<?php


namespace App\API\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ranking extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable=[
        'id',
        'game_id',
        'user_id',
        'score',
        'time',
    ];

    public function user() : HasMany
    {
        return $this->hasMany(User::class);
    }
    public function game() : HasMany
    {
        return $this->hasMany(Game::class);
    }
}
