<?php

namespace App\API\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable=[
        'id',
        'type',
        'title',
        'goals',
    ];

    public function questions() : HasMany
    {
        return $this->hasMany(Question::class);
    }

    public function ranking() : HasMany
    {
        return $this->hasMany(Ranking::class);
    }
}
