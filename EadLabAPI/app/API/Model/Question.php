<?php


namespace App\API\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Question extends Model
{
    use SoftDeletes;
    protected $primaryKey = 'id';

    public $incrementing = true;

    protected $fillable = [
        'id',
        'game_id',
        'question',
        'alternative_0',
        'alternative_1',
        'alternative_2',
        'alternative_3',
        'answer',
    ];

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }
}
