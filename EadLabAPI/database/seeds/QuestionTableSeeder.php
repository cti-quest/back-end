<?php

use Illuminate\Database\Seeder;
use App\API\Model\Question;
use App\API\Model\Game;

class QuestionTableSeeder extends Seeder
{
    public function run()
    {
        Question::create([
            'game_id' => Game::first()->id,
            'question' => 'Seu produto/serviço deve procurar sempre atender as necessidades do:',
            'alternative_0' => 'gestor da produção',
            'alternative_1' => 'cliente',
            'alternative_2' => 'líder da produção',
            'alternative_3' => 'dono da empresa',
            'answer' => '2',
        ]);

        Question::create([
            'game_id' => Game::first()->id,
            'question' => 'É considerado um tipo de capacitação:',
            'alternative_0' => 'curso',
            'alternative_1' => 'produção de um pedido',
            'alternative_2' => 'prestação de um serviço',
            'alternative_3' => 'manutenção de equipe',
            'answer' => '0',
        ]);
        Question::create([
            'game_id' => Game::first()->id,
            'question' => 'Em quais locais podemos encontrar a PQ:',
            'alternative_0' => 'quadros de aviso / placas',
            'alternative_1' => 'quadros de aviso / site',
            'alternative_2' => 'quadros de aviso / email',
            'alternative_3' => 'email / site',
            'answer' => '1',
        ]);
        Question::create([
            'game_id' => Game::first()->id,
            'question' => 'A premissa da PQ é baseada no:',
            'alternative_0' => 'compromisso da empresa com as partes interessadas',
            'alternative_1' => 'compromisso da empresa com os clientes',
            'alternative_2' => 'compromisso das partes interessadas com todos',
            'alternative_3' => 'compromisso da empresa com os colaboradores',
            'answer' => '0',
        ]);
        Question::create([
            'game_id' => Game::first()->id,
            'question' => 'As 4 partes que a Política da Qualidade deve atender são:',
            'alternative_0' => 'satisfação do cliente/ requisitos aplicáveis/ treinamento / melhoria contínua',
            'alternative_1' => 'satisfação do cliente/ requisitos clientes/ treinamento / melhoria contínua',
            'alternative_2' => 'satisfação do cliente/ requisitos clientes/ capacitação / melhoria contínua',
            'alternative_3' => 'satisfação do cliente/ requisitos aplicáveis/ capacitação / melhoria contínua',
            'answer' => '3',
        ]);

    }
}
