<?php

use Illuminate\Database\Seeder;
use App\API\Model\User;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@adm.com.br',
            'password' => Hash::make('123'),
            'total_score' => 100000,
            'role' => 'admin',
        ]);

        User::create([
            'name' => 'Fulano de Fulanoia',
            'email' => 'fulano@fulano.com.br',
            'password' => Hash::make('123'),
            'total_score' => 000,
        ]);

        User::create([
            'name' => 'Mario',
            'email' => 'mario@mario.com.br',
            'password' => Hash::make('123'),
            'total_score' => 185,
        ]);
    }
}
