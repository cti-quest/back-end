<?php

use Illuminate\Database\Seeder;
use App\API\Model\Ranking;
use App\API\Model\Game;
use App\API\Model\User;

class RankingTableSeeder extends Seeder
{
    public function run()
    {
        Ranking::create([
            'game_id' => Game::first()->id,
            'user_id' => User::firstWhere('name', 'Admin')->id,
            'score' => 10000,
            'time' => '2020-11-19 00:10:20',
        ]);

        Ranking::create([
            'game_id' => Game::first()->id,
            'user_id' => User::firstWhere('name', 'Fulano de Fulanoia')->id,
            'score' => 185,
            'time' => '2020-11-19 00:05:50',
        ]);

        Ranking::create([
            'game_id' => Game::first()->id,
            'user_id' => User::firstWhere('name', 'Mario')->id,
            'score' => 000,
            'time' => '2020-11-19 00:30:42',
        ]);
    }
}
