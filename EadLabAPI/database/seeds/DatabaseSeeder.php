<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            GameTableSeeder::class,
            QuestionTableSeeder::class,
            UserTableSeeder::class,
            RankingTableSeeder::class,
        ]);
    }
}
