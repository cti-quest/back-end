<?php

use Illuminate\Database\Seeder;
use App\API\Model\Game;

class GameTableSeeder extends Seeder
{
    public function run()
    {
        Game::create([
            'type' => 'Quiz',
            'title' => 'Política de qualidade',
            'goals' => 'Responda a questão escolhendo uma das quatro alternativas</br></br>
                        -> Cada resposta correta é equivalente a pontos positivos +</br></br>
                        -> Cada resposta correta é equivalente a pontos negativos -</br></br>
                        O contador de tempo será iniciado assim que apertar o botão JOGAR</br></br>
                        Mas não se preocupe! Não existe um tempo para terminar.O tempo serve para somente calcular o ranking!</br></br>
                        Bom jogo!',
        ]);
    }
}

