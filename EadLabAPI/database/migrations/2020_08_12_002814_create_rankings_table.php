<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRankingsTable extends Migration
{
    public function up()
    {
        Schema::create('rankings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('game_id')
                  ->foreign('game_id')
                  ->references('id')
                  ->on('games');

            $table->bigInteger('user_id')
                  ->foreign('user_id')
                  ->references('id')
                  ->on('users');

            $table->bigInteger('score');
            $table->date('time');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rankings');
    }
}
