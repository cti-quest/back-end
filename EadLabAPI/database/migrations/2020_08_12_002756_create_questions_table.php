<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('game_id')
                ->foreign('game_id')
                ->references('id')
                ->on('games');

            $table->string('question');
            $table->string('alternative_0');
            $table->string('alternative_1');
            $table->string('alternative_2');
            $table->string('alternative_3');
            $table->string('answer');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
